console.log("hello");

// miniactivity
/*function printInput(){
	let nickname = prompt("What is your nickname?");
	console.log(nickname);
};
printInput();
*/

// Functions are lines/blocks of codes that tell our devices/application to perform certain tasks when invoked. Functions are mostly created to create complicated tasks to run several lines of codes in succession.
// They are also used to prevent repetition of typing lines/blocks of codes that perform the same task.
// However, for some use cases, this may not be ideal. for other cases, functions can also process data directly into it instead of relying only on global variables such as prompt(). so minsan, iba ang mas dapat gamitin kesa prompt
// Consider this function:
// we can directly pass data into the function. as for the function below, the data is referred to as the "name" within the printName function. This "name" is what we called as the parameter

/*Parameter
	- it acts as a named variable/container that exists only inside of a function;
	- used to store info that is provided to a function when it is called/invoked; parang placeholder
*/
function printName(name){
	console.log("Hello " + name);
}
printName("Joana");
// "Joana" the information provided directly into the function is called an argument. values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function
// si "name" ang parameter pero si "Joana" ay tiantawag na argument
// parameters exist within the function declaration while the arguments are the actual data/info we call to execute the function. so pinalitan na lahat ni "Joana" lahat ng "name" sa loob nun function
// the same will happen if we change the name into others like in the sample below. malog din sa console as "Hello, John" and "hello Jane"
printName("John");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar);

// yung "name" nga pwede din palitan since develoepr defined naman yung nasa loob, basta sa console.log pareho din yung "" but make sure to follow naming conventions

// can you declare multiple parameters in one function? yes, comma lang to separate them. arguments that will be stored in multiple parameters should also be separated by comma. otherwise magiging undefined siya
// mini-activity

function printNumber(age, year){
	console.log("The numbers passed as arguments are:");
	console.log(age);
	console.log(year);
};
printNumber("37", "1984");

function printName2(friend1, friend2, friend3){
	console.log("My friends are " + friend1 + friend2 + friend3);
}
printName2("Joy, ", "Sam, ", "Yo");

// first parameter = first argument na isesend at babasahin ni console and yung second parameter = second argument, and so on


function checkDivisibilityBy8(num){
	let remainder = num%8;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy8=remainder===0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);
// will return a false for the second example
// We can also do the same using prompt(). However, take note that using prompt() outputs a string data type. String data types are not ideal and cannot be used for mathematical computations

// mini-activity

function checkDivisibilityBy4(num){
	let remainder = num%4;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy4=remainder===0;
	console.log("Is " + num + " divisible by 4?");
	console.log(isDivisibleBy4);
};
checkDivisibilityBy4(64);
checkDivisibilityBy4(14);


function isEven(num){
	console.log(num%2===0); 
};
// isEven(10);

function isOdd(num1){
	console.log(num1%2!== 0);
};

isEven(20);
isOdd(31);

// Functions as arguments
	// function parameters can also accept other functions as arguments. Some complex functions use other functions as arguments to perform complicated tasks/result
	// function within a function siya
	// This will be further discussed when we tackle array methods
function argumentFunction(){
	console.log("this function is passed into another function.");
};

function invokeFunction(functionParameter){
	console.log("This is from invokeFunction");
	functionParameter();
};
invokeFunction(argumentFunction);
 
// adding and removing of parenthesis impacts the output of JS heavily. WHen a function is used with parenthesis, it denotes that involing/calling a function.
// That is why when we call the function, the functionParamter as an argument does not have any parenthesis
// if the function as parameter does not have parenthesis, the function as an argument should have parenthesis to denote that it is a function being passed as an argument

function printFullName (firstName, middleName, lastName){
	console.log(firstName + '' + middleName + '' + lastName);
}
printFullName('Juan', 'Dela', 'Cruz');

// pero pwede ding ganito:

/*
let firstName = "Juan";
let middleName = "dela"
let lastName = "Cruz";
console.log(firstName + "" + middleName + "" + lastName);
*/

// same output kaya lang ito paulit-ulit dahil need mo na naman magdeclare ng new variables pag iba ang deails. unlike sa function, kahit more than one parameter pwede


// return statement
// return statement allows us to output a value from a function to passed to the line/block of code that invoked/called our function
// hindi siya nakikita sa console pero nag-create siya ng value based on the return statement, in this case yung full name
// so whether you post or not yung firstName + middleName + lastName, nacreate na yung value ng return statement
function returnFullName(firstName, middleName, lastName){;
	return firstName + "" + middleName + "" + lastName;
	// console.log("This message will not be printed."); --this line is not printed kasi any line/block of code that comes after the return statement is ignored because it ends the function execution, tapos na yung function
	// tapos sa baba yung partner niya, hindi nakawrap si return, hindi siya maprint sa console
};
console.log(returnFullName("Jeffrey", "Smith", "Bezos"));

// pero pag iwrap mo si return sa console.log, lilitaw or maprint na siya sa console.
// whatever value is returned, it will be the value for that function with use of the arguments passed when the function is called
let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);
// the value returned from a function can also be stored inside a variable
// useful ito kung paulit-ulit mong need icall itong Fullnames na ito, so istore mo na lang siya as variable completeName para un na lang i-call mo next time

// mini-activity
function returnAddress(city, province){
	return city + "" + province;
};
console.log(returnAddress("Malolos City, ", "Bulacan"));

function returnAddress(city, province){
	let fullAddress = city + "," + province;
	return fullAddress;
};
let myAddress = returnAddress("Malolos City ", "Bulacan")
console.log(myAddress);

function printPlayerInfo(username, level, job){
	console.log("Username:" + username);
	console.log("level:" + level);
	console.log("Job:" + job);
};

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);
// returns undefine, bakit?
// because the printPlayerInfo returns nothing. Its task is only to print the messages in the console, not to return any value. Thus, we cannot any value from printPlayerInfo kasi wala tayong nilagay na value sa kanya, so wala siya irereturn
// pag walang return statement, wala kang macall na value.  

